- 👋 Hi, I’m @ImAciidz. GitLab is mainly a mirror for some stuff I put on GitHub, so if you want to get a more accurate picture of what I'm getting up to, I suggest you head to [my GitHub account](https://github.com/imaciidz) instead.
- I'm a L4D speedrunner with some interest in Portal 2, Half-Life, and Titanfall 2.
  - I'm a member of the [L4D Community Team](https://github.com/L4D-Community-Team)
  - I've done extensive QA/testing for (and made minor contributions to) [Source Speedrun Tools](https://mikes.software/sst), a plugin for speedrunners of Source Engine games
  - Admin/supermod of the [L4D leaderboards](https://speedrun.com/left_4_dead) on [speedrun.com](https://www.speedrun.com)
- 👀 I’m interested in *learning* C/C++ and reverse engineering source engine games (primarily L4D series). My current programming knowledge comes from some high school compsci classes (AP Java and also a class on JS/HTML/CSS, so not much lol). I took these classes in 2017-2019, so I've since forgotten just about everything beyond fundamentals.
- 🌱 I’m currently learning...well...not much lmao (severe ADHD and a myriad of other mental health issues goes brr)
- 💞️ I’m looking to collaborate on...~~probably nothing?~~ the L4D Speedrunning Wiki project which can be found [here](https://github.com/l4dsr/l4dsr-wiki)
- 📫 How to reach me - Discord is `aciidz` (formerly `aciidz#4686`), Twitch is [aciidzSR](https://twitch.tv/aciidzSR), YouTube is [@aciidz](https://youtube.com/@aciidz)

~~Click here to see a list of programming-adjacent goals/things I'd like to do in no particular order (pretty much everything listed is presently outside of my capabilities, and probably will be for a long time because my brain is tiny)~~

Just kidding! GitLab doesn't render what I had here on my profile properly. You'll have to go to GitHub to see it instead. Sorry :( 


<!---
ImAciidz/ImAciidz is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
